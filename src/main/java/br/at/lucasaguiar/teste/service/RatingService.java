package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.exception.ValidationErrorException;
import br.at.lucasaguiar.teste.model.Rating;
import br.at.lucasaguiar.teste.repository.RatingRepository;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public Rating create(Rating rating) {
        verifyRating(rating);
        return ratingRepository.save(rating);
    }

    public void verifyRating(Rating rating) {
        if (rating.getRating()<0 || rating.getRating()>4) {
            throw new ValidationErrorException("Rating value not allowed. Value: " + String.valueOf(rating.getRating()));
        }

        var ratings = ratingRepository.findAllByUser(rating.getUser());
        var equalsRating = ratings.stream().filter(rating1 -> rating1.getUser().getId().equals(rating.getUser().getId()) &&
                                            rating1.getMovie().getId().equals(rating.getMovie().getId())).collect(Collectors.toList());

        if (equalsRating.size() > 0) {
            throw new ValidationErrorException("User has already voted for this movie. UserId: " + rating.getUser().getActive() + ". MovieId: " + rating.getMovie().getId());
        }
    }
}
