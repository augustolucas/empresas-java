package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.exception.ObjectNotFoundException;
import br.at.lucasaguiar.teste.model.User;
import br.at.lucasaguiar.teste.model.dto.UserDTO;
import br.at.lucasaguiar.teste.model.enums.Profile;
import br.at.lucasaguiar.teste.repository.UserRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, @Lazy BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    public List<UserDTO> findAll() {
        var users = userRepository.findAll(Sort.by("name"));

        return users.stream()
                .filter(user -> !user.getProfiles().contains(Profile.ADMIN) && user.getActive())
                .map(user -> UserDTO.converter(user))
                .collect(Collectors.toList());
    }

    public List<UserDTO> findAll(Integer offset, Integer pageSize) {
        var pageable = PageRequest.of(offset, pageSize, Sort.by("name"));

        var users = userRepository.findAll(pageable);


        return users.stream()
                .filter(user -> !user.getProfiles().contains(Profile.ADMIN) && user.getActive())
                .map(user -> UserDTO.converter(user))
                .collect(Collectors.toList());
    }

    public User findById(Integer id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("User not found. Id: " + id));
    }

    public UserDTO update(Integer id, UserDTO userDTO) {
        var user = findById(id);

        user.setEmail(userDTO.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setName(userDTO.getName());
        return UserDTO.converter(userRepository.save(user));
    }

    public Integer delete(Integer id) {
        var user = userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("User not found. Id: " + id));
        user.setActive(false);
        userRepository.save(user);
        return id;
    }

    public UserDTO create(UserDTO userDTO) {
        var user = new User();
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setEmail(userDTO.getEmail());
        user.setName(userDTO.getName());

        return UserDTO.converter(userRepository.save(user));
    }



    public void instantiateTestDatabase() {
        var userExists = userRepository.findByEmail("admin@admin.com");
        if (userExists != null) {
            return;
        }
        var user = new User();
        user.setName("admin");
        user.setEmail("admin@admin.com");
        user.setPassword(bCryptPasswordEncoder.encode("admin"));
        user.addProfile(Profile.ADMIN);
        userRepository.save(user);
    }
}
