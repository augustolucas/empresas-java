package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.model.Actor;
import br.at.lucasaguiar.teste.model.Genre;
import br.at.lucasaguiar.teste.model.Movie;
import br.at.lucasaguiar.teste.repository.ActorRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ActorService {
    private final ActorRepository actorRepository;

    public ActorService(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    public Actor findByNameWithMovie(String name, Movie movie){
        var actor = actorRepository.findByName(name);
        if (actor == null) {
            actor = create(name, movie);
        } else {
            actor = update(actor, movie);
        }

        return actor;
    }

    public Actor create(String name, Movie movie){
        var actor = new Actor();
        var movies = new ArrayList<Movie>();
        movies.add(movie);
        actor.setName(name);
        actor.setMovies(movies);

        return actorRepository.save(actor);
    }

    public Actor update(Actor actor, Movie movie){
        var movies = actor.getMovies();
        movies.add(movie);

        return actorRepository.save(actor);
    }
}
