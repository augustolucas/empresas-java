package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.model.Genre;
import br.at.lucasaguiar.teste.model.Movie;
import br.at.lucasaguiar.teste.repository.GenreRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GenreService {
    private final GenreRepository genreRepository;

    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public Genre findByNameWithMovie(String name, Movie movie) {
        var genre = genreRepository.findByName(name);
        if (genre == null){
            genre = create(name, movie);
        } else {
            genre = update(genre, movie);
        }

        return genre;
    }

    public Genre create(String name, Movie movie) {
        var genre = new Genre();
        var movies = new ArrayList<Movie>();
        movies.add(movie);
        genre.setName(name);
        genre.setMovies(movies);
        return genreRepository.save(genre);
    }

    public Genre update(Genre genre, Movie movie) {
        var movies = genre.getMovies();
        movies.add(movie);
        genre.setMovies(movies);

        return genreRepository.save(genre);
    }
}
