package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.exception.ObjectNotFoundException;
import br.at.lucasaguiar.teste.model.Director;
import br.at.lucasaguiar.teste.repository.DirectorRepository;
import org.springframework.stereotype.Service;

@Service
public class DirectorService {
    private final DirectorRepository directorRepository;

    public DirectorService(DirectorRepository directorRepository) {
        this.directorRepository = directorRepository;
    }

    public Director findByName(String name) {
        var director = directorRepository.findByName(name);
        if (director == null){
            director = create(name);
        }

        return director;
    }

    public Director create(String name) {
        var director = new Director();
        director.setName(name);
        return directorRepository.save(director);
    }

}
