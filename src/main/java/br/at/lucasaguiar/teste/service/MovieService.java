package br.at.lucasaguiar.teste.service;

import br.at.lucasaguiar.teste.exception.ObjectNotFoundException;
import br.at.lucasaguiar.teste.model.Movie;
import br.at.lucasaguiar.teste.model.Rating;
import br.at.lucasaguiar.teste.model.dto.MovieDTO;
import br.at.lucasaguiar.teste.model.dto.RatingDTO;
import br.at.lucasaguiar.teste.repository.MovieRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {

    private final MovieRepository movieRepository;
    private final DirectorService directorService;
    private final GenreService genreService;
    private final ActorService actorService;
    private final EntityManager entityManager;
    private final UserService userService;
    private final RatingService ratingService;

    public MovieService(MovieRepository movieRepository, DirectorService directorService, GenreService genreService, ActorService actorService, EntityManager entityManager1, UserService userService, RatingService ratingService) {
        this.movieRepository = movieRepository;
        this.directorService = directorService;
        this.genreService = genreService;
        this.actorService = actorService;
        this.entityManager = entityManager1;
        this.userService = userService;
        this.ratingService = ratingService;
    }

    public MovieDTO create(MovieDTO movieDTO) {
        var movie = new Movie();
        movie.setName(movieDTO.getName());
        movie.setDescription(movieDTO.getDescription());
        movie.setDirector(directorService.findByName(movieDTO.getDirector()));
        movie.setDuration(movieDTO.getDuration());
        movie.setYear(movieDTO.getYear());

        Movie movieSaved = movieRepository.save(movie);
        Movie finalMovieSaved = movieSaved;

        movie.setGenre(movieDTO.getGenres().stream().map(genreName -> genreService.findByNameWithMovie(genreName, finalMovieSaved)).collect(Collectors.toList()));
        movie.setActors(movieDTO.getActors().stream().map(actorName -> actorService.findByNameWithMovie(actorName, finalMovieSaved)).collect(Collectors.toList()));

        movieSaved = movieRepository.save(movie);

        return MovieDTO.convert(movieSaved);
    }

    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Movie not found. Id: " + id));
    }

    public List<MovieDTO> findAllMiddleware(String filterBy, Integer offset, Integer pageSize) {
        if(filterBy != null) {
            return findAllFilter(filterBy, offset, pageSize);
        }
        if (offset != null && pageSize != null) {
            return findAllPagination(offset, pageSize);
        }

        return findAll();
    }

    public List<MovieDTO> findAllPagination(Integer offset, Integer pageSize) {
        var pageable = PageRequest.of(offset, pageSize, Sort.by("name").and(Sort.by("ratingAverage")));
        List<Movie> movies = movieRepository.findAll(pageable).stream().collect(Collectors.toList());

        var moviesDTO = movies.stream().map(movie -> MovieDTO.convert(movie)).collect(Collectors.toList());

        return moviesDTO;
    }

    public List<MovieDTO> findAllFilter(String filterBy, Integer offset, Integer pageSize) {
       var query = entityManager.createQuery("SELECT DISTINCT m FROM Movie m JOIN m.director d JOIN m.genres g " +
                "JOIN m.actors a JOIN m.genres g " +
                "WHERE d.name = '" + filterBy + "' OR m.name = '" + filterBy + "' OR a.name = '" + filterBy + "' OR g.name = '" + filterBy + "' " +
                "ORDER BY m.rating ASC, m.name");

        if (offset != null && pageSize != null) {
            query.setFirstResult(offset * pageSize);
            query.setMaxResults(pageSize);
        }
       List<Movie> movies = query.getResultList();

        var moviesDTO = movies.stream().map(movie -> MovieDTO.convert(movie)).collect(Collectors.toList());

        return moviesDTO;
    }

    public List<MovieDTO> findAll() {
        var movies = movieRepository.findAll(Sort.by("name").and(Sort.by("ratingAverage"))).stream().collect(Collectors.toList());
        var moviesDTO = movies.stream().map(movie -> MovieDTO.convert(movie)).collect(Collectors.toList());
        return moviesDTO;
    }

    public RatingDTO vote(Integer movieId, Integer userId, Float ratingValue) {
        var user = userService.findById(userId);
        var movie = findById(movieId);

        var rating = new Rating();
        rating.setRating(ratingValue);
        rating.setMovie(movie);
        rating.setUser(user);

        rating = ratingService.create(rating);

        movie.addRating(rating);
        movieRepository.save(movie);

        return RatingDTO.convert(rating);
    }
}
