package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
}
