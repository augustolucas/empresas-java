package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DirectorRepository extends JpaRepository<Director, Integer> {

    Director findByName(String directorName);
}
