package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.Rating;
import br.at.lucasaguiar.teste.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository  extends JpaRepository<Rating, Integer> {
    List<Rating> findAllByUser(User user);
}
