package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepository extends JpaRepository<Actor, Integer> {
    Actor findByName(String name);
}
