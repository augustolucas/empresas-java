package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GenreRepository  extends JpaRepository<Genre, Integer> {
    Genre findByName(String name);
}
