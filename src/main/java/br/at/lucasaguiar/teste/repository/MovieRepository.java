package br.at.lucasaguiar.teste.repository;

import br.at.lucasaguiar.teste.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
