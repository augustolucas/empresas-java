package br.at.lucasaguiar.teste;

import br.at.lucasaguiar.teste.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;

@EnableWebMvc
@SpringBootApplication
public class TesteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteApplication.class, args);
	}

	private final UserService userService;

	public TesteApplication(UserService userService) {
		this.userService = userService;
	}

	@PostConstruct
	private void init() {
		userService.instantiateTestDatabase();
	}
}
