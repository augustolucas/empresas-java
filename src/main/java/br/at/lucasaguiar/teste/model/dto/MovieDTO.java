package br.at.lucasaguiar.teste.model.dto;

import br.at.lucasaguiar.teste.model.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MovieDTO {

    private Integer id;
    @NotNull(message = "Name cannot be null")
    private String name;
    @NotNull(message = "Description cannot be null")
    private String description;
    @NotNull(message = "Year cannot be null")
    private Integer year;
    @NotNull(message = "Duration cannot be null")
    private Integer duration;
    @NotNull(message = "Director cannot be null")
    private String director;
    @NotNull(message = "Genres cannot be null")
    private List<String> genres = new ArrayList<>();
    @NotNull(message = "Actors cannot be null")
    private List<String> actors = new ArrayList<>();
    private List<RatingDTO> ratings = new ArrayList<>();
    private Float ratingAverage;
    private Boolean active;


    public static MovieDTO convert(Movie movie) {
        var movieDTO = new MovieDTO();
        movieDTO.setId(movie.getId());
        movieDTO.setName(movie.getName());
        movieDTO.setDescription(movie.getDescription());
        movieDTO.setYear(movie.getYear());
        movieDTO.setDuration(movie.getDuration());
        movieDTO.setDirector(movie.getDirector().getName());
        movieDTO.setGenres(movie.getGenre().stream().map(genre -> genre.getName()).collect(Collectors.toList()));
        movieDTO.setActors(movie.getActors().stream().map(actor -> actor.getName()).collect(Collectors.toList()));
        movieDTO.setRatings(movie.getRatings().stream().map(rating -> RatingDTO.convert(rating)).collect(Collectors.toList()));
        movieDTO.setActive(movie.getActive());
        movieDTO.setRatingAverage(movie.getRatingAverage());
        return movieDTO;
    }


    public MovieDTO() {
    }

    public MovieDTO(Integer id, String name, String description, Integer year, Integer duration, String director, List<String> genres, List<String> actors, List<RatingDTO> ratings, Boolean active) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.year = year;
        this.duration = duration;
        this.director = director;
        this.genres = genres;
        this.actors = actors;
        this.ratings = ratings;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public List<RatingDTO> getRatings() {
        return ratings;
    }

    public void setRatings(List<RatingDTO> ratings) {
        this.ratings = ratings;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Float getRatingAverage() {
        return ratingAverage;
    }

    public void setRatingAverage(Float ratingAverage) {
        this.ratingAverage = ratingAverage;
    }
}
