package br.at.lucasaguiar.teste.model.dto;


import br.at.lucasaguiar.teste.model.Rating;

public class RatingDTO {
    private Integer id;
    private Integer user;
    private Integer movie;
    private Float rating;

    public RatingDTO() {
    }

    public RatingDTO(Integer id, Integer user, Integer movie, Float rating) {
        this.id = id;
        this.user = user;
        this.movie = movie;
        this.rating = rating;
    }

    public static RatingDTO convert(Rating rating) {
        var ratingDTO = new RatingDTO(rating.getId(), rating.getUser().getId(), rating.getMovie().getId(), rating.getRating());
        return ratingDTO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getMovie() {
        return movie;
    }

    public void setMovie(Integer movie) {
        this.movie = movie;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }
}
