package br.at.lucasaguiar.teste.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "year")
    private Integer year;

    @Column(name = "duration")
    private Integer duration;

    @ManyToOne()
    @JoinColumn(name = "director_id")
    private Director director;

    @ManyToMany(mappedBy = "movies")
    private List<Genre> genres = new ArrayList<>();

    @ManyToMany(mappedBy = "movies")
    private List<Actor> actors = new ArrayList<>();

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Rating> ratings = new ArrayList<>();

    @Column(name = "rating_average")
    private float ratingAverage = 0;

    @Column(name = "active")
    private Boolean active;

    public Movie() {
        this.active = true;
    }

    public Movie(Integer id, String name, String description, Integer year, Integer duration, Director director, List<Genre> genres, List<Actor> actors, List<Rating> ratings) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.year = year;
        this.duration = duration;
        this.director = director;
        this.genres = genres;
        this.actors = actors;
        this.ratings = ratings;
        this.active = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public List<Genre> getGenre() {
        return genres;
    }

    public void setGenre(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setRatingAverage() {
        if (ratings.size() == 0) {
            ratingAverage = 0;
            return;
        }
        var ratingsArr = ratings.stream().map(rating -> rating.getRating()).collect(Collectors.toList());
        double sum = 0;
        for (double ratingValue: ratingsArr) {
            sum += ratingValue;
        }
        ratingAverage = (float) (sum/ratingsArr.size());
    }

    public float getRatingAverage() {
        setRatingAverage();
        return ratingAverage;
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
        setRatingAverage();
    }
}
