package br.at.lucasaguiar.teste.model.dto;

import br.at.lucasaguiar.teste.model.Rating;
import br.at.lucasaguiar.teste.model.User;
import br.at.lucasaguiar.teste.model.enums.Profile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDTO {
    private Integer id;

    @NotNull(message = "Name cannot be null")
    private String name;
    @NotNull(message = "Email cannot be null")
    private String email;
    @JsonIgnore
    @NotNull(message = "Password cannot be null")
    private String password;
    private List<RatingDTO> ratings = new ArrayList<>();
    private Set<Integer> profiles = new HashSet<>();
    private Boolean active;

    public static UserDTO converter(User user) {
        var ratingsDTO = user.getRatings().stream().map(rating -> RatingDTO.convert(rating)).collect(Collectors.toList());
        var userDTO = new UserDTO(user.getId(), user.getName(),user.getEmail(), user.getPassword(), ratingsDTO, user.getActive());

        user.getProfiles().stream().forEach(profile -> userDTO.addProfile(profile));

        return userDTO;
    }

    public UserDTO() {
        setActive(true);
        addProfile(Profile.CUSTOMER);
    }

    public UserDTO(Integer id, String name, String email, String password, List<RatingDTO> ratings, Boolean active) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.ratings = ratings;
        this.active = active;
        setActive(true);
        addProfile(Profile.CUSTOMER);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<RatingDTO> getRatings() {
        return ratings;
    }

    public void setRatings(List<RatingDTO> ratings) {
        this.ratings = ratings;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Profile> getProfiles() {
        return profiles.stream().map(profile -> Profile.toEnum(profile)).collect(Collectors.toSet());
    }

    public void addProfile(Profile profile) {
        profiles.add(profile.getCode());
    }
}
