package br.at.lucasaguiar.teste.exception;

public class ValidationErrorException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ValidationErrorException(String msg) {
        super(msg);
    }

    public ValidationErrorException(String msg, Throwable cause) {
        super(msg,cause);
    }
}