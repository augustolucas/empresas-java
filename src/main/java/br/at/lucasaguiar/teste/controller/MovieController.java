package br.at.lucasaguiar.teste.controller;
import br.at.lucasaguiar.teste.model.dto.MovieDTO;
import br.at.lucasaguiar.teste.model.dto.RatingDTO;
import br.at.lucasaguiar.teste.service.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/movie")
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping
    public ResponseEntity<MovieDTO> create(@RequestBody MovieDTO movieDTO) {
        return ResponseEntity.ok().body(movieService.create(movieDTO));
    }

    @GetMapping
    public ResponseEntity<List<MovieDTO>> get(@RequestParam(required = false) String filterParam,
                                              @RequestParam(required = false) Integer offset,
                                              @RequestParam(required = false) Integer pageSize) {


        return ResponseEntity.ok().body(movieService.findAllMiddleware(filterParam, offset, pageSize));
    }

    @PostMapping("/{movieId}")
    public ResponseEntity<RatingDTO> vote(@PathVariable Integer movieId,
                                          @RequestParam Integer userId,
                                          @RequestParam Float rating) {


        return ResponseEntity.ok().body(movieService.vote(movieId, userId, rating));
    }

}
