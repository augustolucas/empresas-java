package br.at.lucasaguiar.teste.controller;

import br.at.lucasaguiar.teste.model.dto.UserDTO;
import br.at.lucasaguiar.teste.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value="create")
    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok().body(userService.create(userDTO));
    }


    @ApiOperation(value="findAll")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping()
    public ResponseEntity<List<UserDTO>> findAll(@RequestParam(required = false) Integer offset, @RequestParam(required = false) Integer pageSize) {
        if (offset != null && pageSize != null) {
            return ResponseEntity.ok().body(userService.findAll(offset, pageSize));
        }
        return ResponseEntity.ok().body(userService.findAll());
    }

    @ApiOperation(value="update")
    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> update(@PathVariable Integer id, @RequestBody UserDTO userDTO){
        return ResponseEntity.ok().body(userService.update(id, userDTO));
    }

    @ApiOperation(value="delete")
    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> delete(@PathVariable Integer id) {
        return ResponseEntity.ok().body(userService.delete(id));
    }

}
